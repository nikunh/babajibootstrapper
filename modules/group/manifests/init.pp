class group {
  group {
    'root':
      ensure  => 'present',
      gid     => '0';

    'devops':
      ensure => 'present',
      gid => '11999';

    'ubuntu':
      ensure => 'present',
      gid => '1000';

    'dba':
      ensure => 'present',
      gid => '12001';

    'data':
      ensure => 'present',
      gid => '12002';

    'qa':
      ensure => 'present',
      gid => '12003';

    'dev':
      ensure => 'present',
      gid => '12004';

    'techusers':
      ensure => 'present',
      gid => '12005';

    'ba':
      ensure => 'present',
      gid => '12006';

    'newrelic':
      ensure => 'present',
      gid => '12007';

    'rvm':
      ensure => 'present',
      gid => '12022';



  }
}
