# vim: set ts=2 sw=2 expandtab:
class user::virtual {
  #@user { 'ubuntu':
    #uid         => '1000',	
    #gid         => '1000',
    #comment     => 'ubuntu headless ',
    #ensure      => 'present',
    #password    => 'sdfadsfadf',
    #home        => '/home/ubuntu',
    #shell       => '/bin/bash',
    #managehome  => true,
  #}
  #@ssh_authorized_key {
    #"ubuntu":
      #ensure  => present,
      #type    => "ssh-rsa",
      #key     => ";alkdjfa;ldkjfa;dlkfjad;lfkjads;flkjasd;flkads;fljk",
      #user    => "ubuntu";
  #}

  @user { 'user1':
    uid         => '10002',
    gid         => 'devops',
    comment     => 'user1',
    ensure      => 'present',
    password    => 'a;lksdjfa;ldkfja;dlkfj',
    home        => '/home/user1',
    shell       => '/bin/zsh',
    managehome  => true,
    groups    => ['devops'],
  }
 @ssh_authorized_key {
    "user1":
      ensure  => present,
      type    => "ssh-rsa",
      key     => "al;kdsfja;ldkfjadflkja",
      user    => "user1";
  }
  @user { 'user2':
    uid         => '10003',
    gid         => 'devops',
    comment     => 'user1',
    ensure      => 'present',
    password    => 'a;lksdjfa;ldkfja;dlkfj',
    home        => '/home/user2',
    shell       => '/bin/zsh',
    managehome  => true,
    groups    => ['devops'],
  }
 @ssh_authorized_key {
    "user2":
      ensure  => present,
      type    => "ssh-rsa",
      key     => "alkdjfadlkjfa;ldkfja;dklfj",
      user    => "user2";
  }
  @user { 'user3':
    uid         => '10004',
    gid         => 'devops',
    comment     => 'user3',
    ensure      => 'present',
    password    => 'a;lksdjfa;ldkfja;dlkfj',
    home        => '/home/user3',
    shell       => '/bin/zsh',
    managehome  => true,
    groups    => ['devops'],
  }
 @ssh_authorized_key {
    "user3":
      ensure  => present,
      type    => "ssh-rsa",
      key     => "akldjfa;dlskfja;dlfkj",
      user    => "user3";
  }

#Below is the end bracket. Do not remove this.
}

