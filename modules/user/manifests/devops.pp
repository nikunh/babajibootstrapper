# vim: set ts=2 sw=2 expandtab:
class user::devops inherits user::virtual {
  realize(
	User['user1'], Ssh_authorized_key['user1'],
	User['user2'], Ssh_authorized_key['user2'],
  )
}

