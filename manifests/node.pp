stage { 'first': before => Stage[main] }
stage { 'last': require => Stage[main] }

class all {
  case $operatingsystem {
    debian, ubuntu: {
      include group
      include user::devops
    }

    default: {
      err('We Support only ubuntu now, Please talk to platform team for more details')
    }

  }

#ec2instance_tagz_product is a custom facter that provides the instance application group from respective aws tag

  case $ec2instance_tagz_product {
    ##########
    'Feature': {
        include user::feature
    }
    ##########
    'Develop': { 
      include user::develop
    }
    ##########
    'Staging': {
      include user::staging
    }
    ##########
    'Release':{
      include user::release
    }
    ##########
    'Production': {
      include user::production
    }
  }
}

node "test-orch.mgmt.orch.sg" {
      include group
      include user::devops
      include user::production
####### Important variables
      $enable_puppetdb = true
      $enable_hiera = true
      $master_host = $::fqdn
      $puppetdb_host = $::fqdn
      $postgres_host = $::fqdn
      $hiera_datadir = "/etc/puppet/hieradata"
      $hiera_keysdir = "/etc/puppet/keys"
      $enable_aws_module = true

####### Puppetdb installation, if $enable_puppetdb above is true
      if $enable_puppetdb {
        class { 'puppetdb::globals':
          version => '2.3.8-1puppetlabs1',
        }
        class { 'puppetdb::master::config':
          puppetdb_server => $puppetdb_host,
          puppet_service_name => 'puppetmaster',
        }
        class { 'puppetdb::database::postgresql':
          listen_addresses => $postgres_host,
        }
        class { 'puppetdb::server':
          database_host => $postgres_host,
        }
      }

####### Hiera/ehiera installation, if $enable_hiera above is true
      if $enable_hiera {
        file {
          "/etc/puppet/hieradata":
            ensure => 'directory',
            owner  => 'root',
            group  => 'root',
            mode   => '0755',
            notify => File['/etc/puppet/hieradata/common.eyaml'],
        }
        file {
          "/etc/puppet/hieradata/common.eyaml":
            ensure  => 'file',
            source  => 'puppet:///modules/bootstrap/common.eyaml',
            owner   => 'root',
            group   => 'root',
            replace => 'no',
            mode    => '0755',
            notify  => Class['hiera'],
        }
        class { 'hiera':
          hierarchy => [
            "%{::clientcert}",
            'common',
            '%{::ec2instance_tagz_env}',
          ],
          logger          => 'console',
          merge_behavior  => 'deep',
          eyaml           => true,
          eyaml_extension => 'eyaml',
          datadir_manage  => false,
          datadir         => $hiera_datadir,
          create_keys     => true,
          keysdir         => $hiera_keysdir,
        }
      }

####### Puppet aws module installation, if $enable_aws_module above is true
    if $enable_aws_module {
      $aws_id = hiera('aws_id',{})
      $aws_secret = hiera('aws_secret', {})
      $aws_credentials = "[default]\naws_access_key_id = $aws_id\naws_secret_access_key = $aws_secret"

          #content => "[default]\naws_access_key_id = ${hiera('aws_id')}\naws_secret_access_key = ${hiera('aws_secret')}",
      package {
        "aws-sdk-core":
          ensure   => 'installed',
          provider => 'gem',
      }
      package {
        "retries":
          ensure   => 'installed',
          provider => 'gem',
      }
      file {
        "/root/.aws":
          ensure => 'directory',
          owner  => 'root',
          group  => 'root',
          mode   => '0755',
          notify => File['/root/.aws/credentials'],
      }
      file {
        "/root/.aws/credentials":
          ensure  => 'file',
          owner   => 'root',
          content => $aws_credentials,
          group   => 'root',
          mode    => '0755',
          require => File['/etc/puppet/hieradata/common.eyaml'],
      }
    }

####### This file here is to show puppet works
      file { "/tmp/TouchedByBabajiKapiche":
          ensure => file,
          owner  => "root",
          group  => "root",
          mode   => 0644,
          content => "Hello, World\n",
        }
}
