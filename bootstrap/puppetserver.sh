#!/bin/bash
#if [ $# -eq 0 ]
#  then
#    echo "No hostname supplied"
#    exit
#fi
#env=$1
#echo $env
#hostname=`hostname`
echo "press ctrl^c and set your hostname first: "
read answer
if ! dpkg -l 'puppetmaster'; then
    [ -e ./puppetlabs-release-trusty.deb ] && echo "puppetlabs-release-trusty.deb present not downloading ..." || wget http://apt.puppetlabs.com/puppetlabs-release-trusty.deb
    dpkg -i puppetlabs-release-trusty.deb
    apt-get update -y
    apt-get -y install puppetmaster puppet
else
    echo "puppetmaster seems to be already installed, use force if not"
fi
#sed -i "s/$hostname/$env/g" /etc/hostname
sed -i "s/no/yes/g"  /etc/default/puppetmaster
#echo "127.0.0.1  puppet.example.net    puppet" >>  /etc/hosts
#echo "172.31.44.238  puppet.example.net    puppet" >>  /etc/hosts
[ -e /etc/puppet/babaji ] && cd /etc/puppet/babaji; git pull || git clone git@bitbucket.org:nikunh/babajibootstrapper.git /etc/puppet/babaji/
rm -f /etc/puppet/puppet.conf
ln -s /etc/puppet/babaji/bootstrap/puppet.conf /etc/puppet/puppet.conf
rm -rf /etc/puppet/modules
ln -s /etc/puppet/babaji/modules /etc/puppet/modules
rm -rf /etc/puppet/manifests
ln -s /etc/puppet/babaji/manifests /etc/puppet/manifests
/etc/init.d/puppetmaster stop
rm -rf /var/lib/puppet/ssl/*
chown -R puppet:puppet /var/lib/puppet
chown -R puppet:puppet /etc/puppet/
#export CONFDIR=/root/babaji/ && puppet master --confdir=$CONFDIR --vardir=$CONFDIR/var --modulepath=$CONFDIR/modules --pluginsync --no-daemonize --debug --verbose
#export CONFDIR=/root/babaji/ && puppet agent --confdir=$CONFDIR --vardir=$CONFDIR/var server=my-pp-master --test
#puppet cert --confdir=/home/bewang/.puppet --vardir=/home/bewang/.puppet/var list
#puppet cert --confdir=/home/bewang/.puppet --vardir=/home/bewang/.puppet/var sign my-pp-agent-host-name

