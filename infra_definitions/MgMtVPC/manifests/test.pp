########################
# Instance definitions #
########################
#free ubuntu aws ami: ami-21d30f42
#ubuntu orch image:  ami-6625ef05
#user_data                   => template('cloudinittemplate.userdata.sh'),
ec2_instance { 'test-orch1':
  ensure                      => absent,
  availability_zone           => 'ap-southeast-1a',
  image_id                    => 'ami-21d30f42',
  instance_type               => 't2.medium',
  key_name                    => 'mgmt-bastion',
  region                      => 'ap-southeast-1',
  subnet                      => 'mgmt-pri-1a',
  security_groups             => ['mgmt_ssh_access'],
  associate_public_ip_address => false,
  #make sure to set tags correctly for env and name. the env should point to existing environment (common  development  production  staging  test) else puppet will fail
  tags                => {
  env =>  'test',
  name => 'test-orch1',
  product => 'orch',
  application => 'orch-tester',
  },
}

#elb_loadbalancer { 'test-elb':
  #ensure             => 'present',
  #instances          => [],
  #listeners          => [{'instance_port' => '80', 'instance_protocol' => 'HTTP', 'load_balancer_port' => '80', 'protocol' => 'HTTP'}],
  #region             => 'ap-southeast-1',
  #scheme             => 'internal', #'internal'
  #security_groups    => ['niktest-elb-sg'],
  #subnets            => ['prodvpc-private-1a', 'prodvpc-private-1b'],
  #tags               => {'env' => 'production'},
#}
